var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api ";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;

function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        videoId: 'xWEfKKOStTk',
        playerVars: {
            'autoplay': 0,
            'enablejsapi': 1,
            'origin': '',
            'rel': 0,
            'showinfo': 0,
            'mute': 0,
            'controls': 0,
            'loop': 1
        }
    });
}

document.querySelector('#mute').onclick = function() {
    player.playVideo();
    setTimeout(function(){
        document.querySelector('#mute').style.display = "none ";
    }, 1500);
}